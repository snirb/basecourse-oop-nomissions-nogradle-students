package AIF.Enum;

public enum EquipmentEnum {
    WEAPON("Weapon"),
    PHOTOGRAPH("Photograph"),
    SENSOR("Sensor");

    String name;
    EquipmentEnum(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
}
