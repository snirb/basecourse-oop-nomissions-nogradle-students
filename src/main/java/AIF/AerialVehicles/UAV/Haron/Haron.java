package AIF.AerialVehicles.UAV.Haron;

import AIF.AerialVehicles.Exceptions.CannotPerformInMidAirException;
import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;

public abstract class Haron extends UAV {
    protected final static int MAINTENANCE_LIMIT=15000;

    public Haron(Coordinates location) {
        super(location);
    }

    public int getMaintenanceLimit() {
        return MAINTENANCE_LIMIT;
    }
}
