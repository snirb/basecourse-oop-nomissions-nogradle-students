package AIF.AerialVehicles.UAV.Haron;


import AIF.Entities.Coordinates;
import AIF.Enum.EquipmentEnum;

public class Shoval extends Haron{
    protected final static int NUMBER_OF_STATIONS=1;

    public Shoval(Coordinates location) {
        super(location);
        compatibleEquipments =new EquipmentEnum[]{EquipmentEnum.WEAPON,EquipmentEnum.SENSOR};
    }

    public int getNumberOfStations() {
        return NUMBER_OF_STATIONS;
    }
}