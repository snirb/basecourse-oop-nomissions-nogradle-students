package AIF.AerialVehicles.UAV.Hermes;

import AIF.Entities.Coordinates;
import AIF.Enum.EquipmentEnum;

public class Kochav extends Hermes{
    protected final static int NUMBER_OF_STATIONS=5;

    public Kochav(Coordinates location) {
        super(location);
        compatibleEquipments =new EquipmentEnum[]{EquipmentEnum.WEAPON,EquipmentEnum.SENSOR};
    }

    public int getNumberOfStations() {
        return NUMBER_OF_STATIONS;
    }
}
