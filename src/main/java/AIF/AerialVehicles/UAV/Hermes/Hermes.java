package AIF.AerialVehicles.UAV.Hermes;

import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    protected final static int MAINTENANCE_LIMIT=10000;

    public Hermes(Coordinates location) {
        super(location);
    }

    public int getMaintenanceLimit() {
        return MAINTENANCE_LIMIT;
    }
}
