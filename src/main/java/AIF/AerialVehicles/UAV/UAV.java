package AIF.AerialVehicles.UAV;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {

    public UAV(Coordinates location) {
        super(location);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if(!onGround){
            System.out.println("Hovering over: " + location.getLongitude()+","+location.getLatitude());
            passedDistance+=hours*150;
        }
        else{
            System.out.println("UAV is on ground");
            throw new CannotPerformOnGroundException();
        }
    }

}
