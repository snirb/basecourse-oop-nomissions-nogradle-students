package AIF.AerialVehicles.AttackPlanes;


import AIF.Entities.Coordinates;
import AIF.Enum.EquipmentEnum;

public class F15 extends AttackPlanes{
    protected final static int NUMBER_OF_STATIONS=10;

    public F15(Coordinates location) {
        super(location);
        compatibleEquipments =new EquipmentEnum[]{EquipmentEnum.WEAPON,EquipmentEnum.SENSOR};
    }

    public int getNumberOfStations() {
        return NUMBER_OF_STATIONS;
    }
}
