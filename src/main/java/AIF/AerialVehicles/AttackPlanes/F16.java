package AIF.AerialVehicles.AttackPlanes;


import AIF.Entities.Coordinates;
import AIF.Enum.EquipmentEnum;

public class F16 extends AttackPlanes{
    protected final static int NUMBER_OF_STATIONS=7;

    public F16(Coordinates location) {
        super(location);
        compatibleEquipments =new EquipmentEnum[]{EquipmentEnum.WEAPON,EquipmentEnum.PHOTOGRAPH};
    }

    public int getNumberOfStations() {
        return NUMBER_OF_STATIONS;
    }
}