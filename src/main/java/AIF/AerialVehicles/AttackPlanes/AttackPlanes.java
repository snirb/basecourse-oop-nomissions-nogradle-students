package AIF.AerialVehicles.AttackPlanes;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class AttackPlanes extends AerialVehicle {
    protected final static int MAINTENANCE_LIMIT=25000;

    public int getMaintenanceLimit() {
        return MAINTENANCE_LIMIT;
    }

    public AttackPlanes(Coordinates location) {
        super(location);
    }

}
