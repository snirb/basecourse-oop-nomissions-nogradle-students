package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Enum.EquipmentEnum;
import AIF.Equipments.Equipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AerialVehicle {
    protected boolean canFly;
    protected boolean onGround;
    protected Coordinates location;
    protected double passedDistance;
    protected EquipmentEnum[] compatibleEquipments;
    protected List<Equipment> equipments;

    public AerialVehicle(Coordinates location) {
        this.canFly = true;
        this.onGround = true;
        this.location = location;
        this.passedDistance = 0;
        this.equipments= new ArrayList<>();
    }

    public void takeOff() throws CannotPerformInMidAirException, CannotPerformOnGroundException {
        if (canFly && onGround) {
            System.out.println("Taking Off");
            onGround = false;
        } else {
            if (!canFly) {
                System.out.println("need maintenance");
                throw new CannotPerformInMidAirException();
            }
            System.out.println("plane on ground");
            throw new CannotPerformOnGroundException();
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (!onGround) {
            System.out.println("Flying to: " + destination.getLatitude()+","+destination.getLongitude());
            passedDistance += location.distance(destination);
        } else {
            System.out.println("plane is on the ground");
            throw new CannotPerformOnGroundException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (!onGround) {
            System.out.println("Landing");
            onGround = true;
        } else {
            System.out.println("plane is already on the ground");
            throw new CannotPerformOnGroundException();

        }
    }

    public boolean needMaintenance(){
        return this.getMaintenanceLimit() <= passedDistance;
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (onGround) {
            passedDistance = 0;
            System.out.println("Performing maintenance");
        } else {
            System.out.println("plane is in the air");
            throw new CannotPerformInMidAirException();
        }
    }

    public void loadModule(Equipment equipment) throws NoModuleStationAvailableException, ModuleNotCompatibleException {
        boolean isCompatible=false;
        if(equipments.size()>=getNumberOfStations()){
            throw new NoModuleStationAvailableException();
        }
        else{
            for (EquipmentEnum equipmentEnum:compatibleEquipments) {
                if (equipmentEnum.getName().equals(equipmentEnum.getName())) {
                    isCompatible = true;
                    break;
                }
            }
            if(isCompatible){
                equipments.add(equipment);
            }
            else{
                throw new ModuleNotCompatibleException();
            }
        }
    }

    public void activateModule(Class<? extends Equipment> moduleClass,Coordinates targetLocation) throws ModuleNotFoundException, NoModuleCanPerformException {
        Optional<Equipment> equipment=equipments.stream().filter(eq-> eq.getClass().equals(moduleClass)).findAny();
        if(equipment.isEmpty()){
            throw new ModuleNotFoundException();
        }
        else{
            equipment.get().activate();
            System.out.println("Successfully activated " +equipment.get().toString()+" on "+targetLocation.getLongitude()+","+targetLocation.getLatitude());
        }
    }

    public abstract int getMaintenanceLimit();
    public abstract int getNumberOfStations();
}
