package AIF;



import AIF.AerialVehicles.AttackPlanes.F15;
import AIF.AerialVehicles.AttackPlanes.F16;
import AIF.AerialVehicles.UAV.Hermes.Zik;
import AIF.Entities.Coordinates;
import AIF.Equipments.Weapon;

public class App {


    public static void main(String[] args) {

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|Main-Driver-Start|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println();
        F16 f16 = new F16(new Coordinates(0.0,0.0));
        try{
            f16.takeOff();
            f16.flyTo(new Coordinates(26000.0, 2.1));
            f16.land();
            if(f16.needMaintenance()){
                System.out.println("Need Maintenance");
            }
            f16.performMaintenance();
            if(!f16.needMaintenance()){
                System.out.println("Doesn't Need Maintenance Anymore");
           }
           f16.loadModule(new Weapon());
           f16.activateModule(Weapon.class, new Coordinates(26000.0, 2.0));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|Main-Driver-End|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|Distanse-Is-Too-Long-Start|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println();
        F15 f15 = new F15(new Coordinates(0.0,0.0));
        try{
            f15.takeOff();
            f15.flyTo(new Coordinates(260000000.0, 2.1));
            f15.land();
            if(f15.needMaintenance()){
                System.out.println("Need Maintenance");
            }
            f15.performMaintenance();
            if(!f15.needMaintenance()){
                System.out.println("Doesn't Need Maintenance Anymore");
            }
            f15.loadModule(new Weapon());
            f15.activateModule(Weapon.class, new Coordinates(26000.0, 2.0));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|Distanse-Is-Too-Long-End|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|zik-hovering-over-location-successfully:start|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println();
        Zik zik =new Zik(new Coordinates(13.0,13.0));
        try{
            zik.takeOff();
            zik.flyTo(new Coordinates(26000.0, 2.1));

            zik.hoverOverLocation(15);
            zik.land();
            if(zik.needMaintenance()){
                System.out.println("Need Maintenance");
            }
            zik.performMaintenance();
            if(!zik.needMaintenance()){
                System.out.println("Doesn't Need Maintenance Anymore");
            }

            zik.loadModule(new Weapon());
            zik.activateModule(Weapon.class, new Coordinates(26000.0, 2.0));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|zik-hovering-over-location-successfully:end|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|zik-hovering-over-location-not-successfully:start|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println();
        Zik groundedZik =new Zik(new Coordinates(13.0,13.0));
        try{
            groundedZik.takeOff();
            groundedZik.flyTo(new Coordinates(26000.0, 2.1));
            groundedZik.land();
            groundedZik.hoverOverLocation(15);

            if(groundedZik.needMaintenance()){
                System.out.println("Need Maintenance");
            }
            groundedZik.performMaintenance();
            if(!groundedZik.needMaintenance()){
                System.out.println("Doesn't Need Maintenance Anymore");
            }

            groundedZik.loadModule(new Weapon());
            groundedZik.activateModule(Weapon.class, new Coordinates(26000.0, 2.0));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|zik-hovering-over-location-not-successfully:end|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
