package AIF.Equipments;

public class Sensor implements Equipment{
    protected final static int ACTION_RANGE=600;

    public static int getActionRange() {
        return ACTION_RANGE;
    }

    @Override
    public void activate() { }
}
