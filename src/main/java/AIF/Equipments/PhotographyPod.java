package AIF.Equipments;

public class PhotographyPod implements Equipment{
    protected final static int ACTION_RANGE=300;

    public static int getActionRange() {
        return ACTION_RANGE;
    }

    @Override
    public void activate() { }
}
