package AIF.Equipments;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Enum.EquipmentEnum;

public class Weapon implements Equipment{
    protected final static int ACTION_RANGE=1500;
    boolean isLaunched;

    public Weapon(){
        this.isLaunched=false;
    }
    public boolean isLaunched(){
        return this.isLaunched;
    }
    public static int getActionRange() {
        return ACTION_RANGE;
    }

    @Override
    public void activate() throws NoModuleCanPerformException {
        if(!this.isLaunched){
            this.isLaunched=true;
        }
        else{
            throw new NoModuleCanPerformException();
        }
    }

    @Override
    public String toString() {
        return EquipmentEnum.WEAPON.getName();
    }
}
